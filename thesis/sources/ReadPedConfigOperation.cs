﻿using System;
using System.Threading;
using AP.AnyPayPOS.Message.Request;
using AP.AnyPayPOS.Message.Response;
using AP.Core;
using AP.Logging; 
using AP.PXPPaymentContract.Message; 

namespace AP.PXPTmsService.Operation
{
    public class ReadPedConfigOperation : TmsOperation
    {
        public ReadPedConfigOperation(CancellationTokenSource cancellationTokenSource = null)  : base(cancellationTokenSource)
        { 
        }

        protected override IRequest PrepareRequest()
        {
            var request = new GetPedInfoBaseRequest();
            this.InitRequest(request, true);  
            return request;
        }

        protected override void InitConnectionManager()
        {
            base.InitConnectionManager();
            this.ConnectionManager.SendTimeoutSeconds = this.TmsInfo.AnyPaySettings.TimeoutSettings.GetPedInfoTimeoutSeconds;
            this.ConnectionManager.ReceiveTimeoutSeconds = this.TmsInfo.AnyPaySettings.TimeoutSettings.GetPedInfoTimeoutSeconds;

        }
        protected override string StatusFormMessage
        {
            get { return LabelTranslator.Translate(150089); } //Reading PED configuration... Please, wait
        }

        protected override void ProcessServiceResponse(IResponse response)
        {
            try
            {
                if (response == null)
                {
                    this.SetOperationError(); 
                }
                else
                {
                    var baseResponse = response as GetPedInfoBaseResponse;

                    if (baseResponse != null)
                    {
                        if (baseResponse.ResponseCode == PXPPaymentContract.AdministrativeResponseCodes.Success)
                        {
                            this.TmsInfo.PedConfiguration = baseResponse.PedConfiguration;
                            this.TmsInfo.IsPedConfigurationRead = true;
                        }
                        else
                        { 
                            this.LastError = this.CreateError(baseResponse.ResponseMessage); 
                        } 
                    }
                }
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        }
    } 
}
