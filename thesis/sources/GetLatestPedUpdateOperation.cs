﻿using System;
using System.Threading;
using AP.AnyPayPOS.Message.Request;
using AP.AnyPayPOS.Message.Response;
using AP.Core;
using AP.Logging;
using AP.PXPPaymentContract; 
using AP.PXPPaymentContract.Message; 

namespace AP.PXPTmsService.Operation
{
    public class GetLatestPedUpdateOperation : TmsOperation
    {
        public GetLatestPedUpdateOperation(CancellationTokenSource cancellationTokenSource)
            : base(cancellationTokenSource)
        {
        }

        protected override IRequest PrepareRequest()
        {
            var request = new GetLatestPedUpdateBaseRequest();
            this.InitRequest(request, true);
            return request;
        }

        protected override string StatusFormMessage
        {
            get { return LabelTranslator.Translate(150096); } //Download update package
        }

        protected override void ProcessServiceResponse(IResponse response)
        {
            try
            {
                if (response == null)
                {
                    this.SetOperationError(); 
                }
                else
                {
                    var baseResponse = response as GetLatestPedUpdateBaseResponse;

                    if (baseResponse != null)
                    {
                        if (baseResponse.ResponseCode == AdministrativeResponseCodes.Success)
                        {
                            this.TmsInfo.IsLatestUpdateLoaded = true;
                        }
                        else
                        {
                            this.TmsInfo.IsLatestUpdateLoaded = false;
                            this.LastError = this.CreateError(baseResponse.ResponseMessage);
                        }
                    } 
                }
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        }
    }
}
