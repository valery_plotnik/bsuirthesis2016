﻿using AP.Core.Contracts.Operation;

namespace AP.Core.Operation
{
    /// <summary>
    /// Factory class to construct correct operation controller depending on the operation
    /// </summary>
    public static class PosOperationControllerFactory
    {
        public static IPosOperationController Construct(IPosOperation posOperation)
        {
            return new PosOperationController { Operation = posOperation };
        }
    }
}
