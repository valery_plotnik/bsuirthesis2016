﻿using System;
using System.Threading;
using AP.AnyPayPOS.Message.Request;
using AP.AnyPayPOS.Message.Response;
using AP.Core;
using AP.Logging;
using AP.PXPPaymentContract; 
using AP.PXPPaymentContract.Message;  

namespace AP.PXPTmsService.Operation
{
    public class UpdatePedOperation : TmsOperation
    {
        public bool IsForceUpdate { get; set; }
        public UpdatePedOperation(CancellationTokenSource cancellationTokenSource)
            : base(cancellationTokenSource)
        {
        }

        protected override IRequest PrepareRequest()
        {
            var request = new UpdatePedBaseRequest();
            this.InitRequest(request, true);
            request.IsForceUpdate = this.IsForceUpdate;  
            return request;
        }

        protected override string StatusFormMessage
        {
            get { return LabelTranslator.Translate(150103); } //Update credit card terminal
        }

        protected override void ProcessServiceResponse(IResponse response)
        {
            try
            {
                if (response == null)
                {
                    this.SetOperationError(); 
                }
                else
                {
                    var baseResponse = response as UpdatePedBaseResponse;
                    if (baseResponse != null)
                    {
                        if (baseResponse.ResponseCode == AdministrativeResponseCodes.Success)
                        {
                            this.TmsInfo.IsUpdateInstalled = true;
                        }
                        else
                        {
                            this.TmsInfo.IsUpdateInstalled = false;
                            this.LastError = this.CreateError(baseResponse.ResponseMessage);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        } 
    }
}
