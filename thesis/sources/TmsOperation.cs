﻿using System.Threading;
using AP.AnyPayPOS.Message.Request;
using AP.Core;
using AP.Core.WinFormsTouch;
using AP.PXPPaymentContract.DataEntity;
using AP.PXPPaymentContract.DataEntity.Info;
using AP.PXPPaymentContract.WinFormsTouch;
using AP.PXPServices;
using Microsoft.Dynamics.Retail.PaymentSDK;

namespace AP.PXPTmsService.Operation
{
    public class TmsOperation : RetriableOperation
    {
        public TmsOperation(CancellationTokenSource cancellationTokenSource) : base(cancellationTokenSource)
        { 
        }

        public ITmsInfo TmsInfo
        {
            get { return this.AnyPayInfo as ITmsInfo; }
            set { this.AnyPayInfo = value; }
        }  

        protected override string GetUnknownErrorText()
        {
            return LabelTranslator.Translate(150086); /* Unknown error during communication with PED */
        } 

        protected virtual string StatusFormMessage
        {
            get { return string.Empty; } 
        }

        protected override ICancellableStatusForm CreateForm()
        {
            return new frmStatusMessage(this.StatusFormMessage, System.Windows.Forms.MessageBoxIcon.Asterisk);
        }

        protected override bool ValidateSettings()
        {
            bool ret = base.ValidateSettings();
            
            if (ret)
            {
                if (this.TmsInfo.AnyPaySettings.Credentials.PedDeviceType == AnyPayPedDeviceTypes.None)
                {
                    this.CreateError(LabelTranslator.Translate(75006), ErrorCode.InvalidMerchantConfiguration); // PED is not configured. Please, validate POS settings in AX
                    return false;
                }
            }

            return ret;
        }

        protected override void InitRequest(BaseRequest baseRequest, bool generateReference)
        {
            base.InitRequest(baseRequest, generateReference);

            var request = baseRequest as PedBaseRequest;

            if (request != null)
            {
                request.InitFromTmsInfo(this.TmsInfo);
            } 
        } 
    }
}
