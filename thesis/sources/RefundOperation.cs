﻿using AP.AnyPayPOS.Message.Request;
using AP.Core;
using AP.PXPPaymentContract;
using AP.PXPPaymentContract.Message;

namespace AP.PXPPaymentService.Operation
{
    public class RefundOperation : SalesOperation
    {
        protected override IRequest PrepareRequest()
        {
            TransBaseRequest resBaseRequest = base.PrepareRequest() as TransBaseRequest;

            if (resBaseRequest == null)
                return null;

            resBaseRequest.TransactionType = TransactionTypes.Refund;

            return resBaseRequest;
        } 

        protected override string FormHeaderLabel()
        {
            return LabelTranslator.Translate(75051); //Refund: {0} {1}
        }
    }
}
