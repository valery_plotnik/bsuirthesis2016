﻿using System;  
using AP.AnyPayPOS.Message.Request;
using AP.Core;
using AP.PXPPaymentContract;
using AP.PXPPaymentContract.DataEntity;
using AP.PXPPaymentContract.Message;

namespace AP.PXPPaymentService.Operation
{
    public class SalesOperation : CardPaymentOperation
    {
        protected override IRequest PrepareRequest()
        {
            TransBaseRequest resBaseRequest = null;

            switch (this.CardPaymentInfo.CardDataEntryType)
            {
                case CardDataEntryType.Keyed:
                    resBaseRequest = this.BuildKeyedTransRequest();
                    break;

                case CardDataEntryType.Icc:
                    resBaseRequest = this.BuildIccTransRequest();
                    break;
            }

            if (resBaseRequest == null)
                return null;

            resBaseRequest.InitFromCardPaymentInfo(this.CardPaymentInfo);
            
            resBaseRequest.TransactionReference = this.GenerateTransactionReference(this.TransactionInfo);
            resBaseRequest.TransactionType = TransactionTypes.Sale;
            resBaseRequest.AnyPaySettings = this.ConnectionManager.ConnectionSettings;
            
            return resBaseRequest;
        }

        protected string GenerateTransactionReference(ITransactionInfo transactionInfo)
        {
            var reference = this.GenerateReference(transactionInfo);
            return reference;
        } 

        protected override string FormHeaderLabel()
        {
            return LabelTranslator.Translate(75007);//Card payment: {0} {1}
        }
    }
}
