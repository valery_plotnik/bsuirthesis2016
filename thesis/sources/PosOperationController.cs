﻿using System.ComponentModel;
using AP.Core.Contracts.Operation; 

namespace AP.Core.Operation
{
    public class PosOperationController : IPosOperationController
    {
        /// <summary>
        /// Gets or sets the operation that controller performs.
        /// </summary>
        public IPosOperation Operation { get; set; } 

        /// <summary>
        /// <c>BackgroundWorker</c> to run operation asynchronously 
        /// </summary>
        private BackgroundWorker Worker { get; set; }

        /// <summary>
        /// Executes the operation in selected <c>SyncAsyncMode</c> mode.
        /// </summary>
        /// <param name="mode">The operation execution mode</param>
        public virtual void Process(PosOperationExecutionModes mode = PosOperationExecutionModes.Sync)
        {
            switch (mode)
            { 
                case PosOperationExecutionModes.Async: 
                    this.ProcessAsync();
                    break;

                default: 
                    this.ProcessSync();
                    break; 
            } 
        }

        /// <summary>
        /// Executes the operation synchronously.
        /// </summary>
        protected virtual void ProcessSync()
        {
            this.Operation.RunOperation();
            this.ProcessOperationResult();  
        }

        /// <summary>
        /// Executes the operation asynchronously.
        /// </summary>
        protected virtual void ProcessAsync()
        {
            this.InitializeWorker();
            this.Worker.RunWorkerAsync();
        }

        protected void InitializeWorker()
        {
            this.Worker = new BackgroundWorker();
            this.Worker.DoWork += this.RunOperation;
            this.Worker.RunWorkerCompleted += this.Worker_OperationCompleted;
        }

        /// <summary>
        /// Executes the operation with background worker.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="args"><c>DoWorkEventArgs</c> object</param>
        protected void RunOperation(object sender, DoWorkEventArgs args)
        {
            this.Operation.RunOperation();
        }

        /// <summary>
        /// Executes additional operation processing after background worker task is completed.
        /// </summary>s
        /// <param name="sender">Sender object</param>
        /// <param name="e"><c>RunWorkerCompletedEventArgs</c> object</param>
        protected virtual void Worker_OperationCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ProcessOperationResult();
        }

        /// <summary>
        /// Operation result additional processing.
        /// </summary>
        protected virtual void ProcessOperationResult()
        {
        }
    }
}
