﻿using System; 
using System.Net;
using System.Net.Sockets; 

namespace AP.AnyPayPOS.SocketTools
{ 
    public delegate void ReadCallBack(IAsyncResult asyncResult); 

    public sealed class TcpClientWrapper  
    { 
        public int BufferSize { get; set; }

        public TcpClient TcpClient { get; protected set; }

        public TcpClientWrapper(TcpClient tcpClient)
        {
            this.TcpClient = tcpClient; 
        }

        public void SendRequest(byte[] request)
        {  
            this.WriteBytes(request);
        } 

        public IAsyncResult BeginRead(ReadCallBack readCallback)
        {
            NetworkStream stream = this.TcpClient.GetStream();
            var clientState = new TcpClientState(stream, new Byte[this.BufferSize]); 
            var result = stream.BeginRead(clientState.Buffer, 0, clientState.Buffer.Length, new AsyncCallback(readCallback), clientState); 
            return result;
        } 

        private void WriteBytes(byte[] bytesToSend)
        {
            NetworkStream stream = this.TcpClient.GetStream();
            stream.Write(bytesToSend, 0, bytesToSend.Length);
            stream.Flush();
        } 
         
        public void Connect(IPEndPoint ipEndPoint)
        {
            this.TcpClient.Connect(ipEndPoint);
        }

        public void Close(bool closeStream = false)
        {
            if (closeStream)
            {
                this.TcpClient.GetStream().Close();
            }

            this.TcpClient.Close();
        }

        public bool Connected
        {
            get { return this.TcpClient.Connected; }
        } 
    }
}
