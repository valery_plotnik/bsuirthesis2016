﻿using System;
using System.Threading;
using AP.AnyPayPOS.Message.Request;
using AP.AnyPayPOS.Message.Response;
using AP.Core;
using AP.Logging;
using AP.PXPPaymentContract; 
using AP.PXPPaymentContract.Message; 

namespace AP.PXPTmsService.Operation
{
    public class TestPedStateOperation : TmsOperation
    {
        public TestPedStateOperation(CancellationTokenSource cancellationTokenSource) : base(cancellationTokenSource)
        {
        }

        protected override IRequest PrepareRequest()
        {
            var request = new QueryPedBaseRequest();
            this.InitRequest(request, true); 
            return request;
        }

        protected override string StatusFormMessage
        {
            get { return LabelTranslator.Translate(150087); } //Check if Credit Card Terminal is free...
        }

        protected override void InitConnectionManager()
        {
            base.InitConnectionManager();
            this.ConnectionManager.SendTimeoutSeconds = this.TmsInfo.AnyPaySettings.TimeoutSettings.QueryPedTimeoutSeconds;
            this.ConnectionManager.ReceiveTimeoutSeconds = this.TmsInfo.AnyPaySettings.TimeoutSettings.QueryPedTimeoutSeconds;
        }

        protected override void ProcessServiceResponse(IResponse response)
        {
            try
            {
                if (response == null)
                {
                    this.TmsInfo.PedState = PedStates.Unknown;
                    this.TmsInfo.CanPxpServiceBeContacted = false;
                    this.SetOperationError(); 
                }
                else
                {
                    var pedResponse = response as QueryPedBaseResponse;

                    if (pedResponse != null)
                    {
                        this.TmsInfo.PedState = pedResponse.PedState;
                        this.TmsInfo.PedStateStatus = pedResponse.Status;
                        this.TmsInfo.CanPxpServiceBeContacted = true;
                    }
                }
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        }
    }
}
