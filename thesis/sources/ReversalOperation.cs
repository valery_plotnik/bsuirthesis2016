﻿using AP.AnyPayPOS.Message.Request;
using AP.Core;
using AP.PXPPaymentContract.Message;
using Microsoft.Dynamics.Retail.PaymentSDK; 

namespace AP.PXPPaymentService.Operation
{
    public class ReversalOperation : SalesOperation
    {
        protected override bool ValidateCardPaymentInfo()
        {
            bool ret = base.ValidateCardPaymentInfo();

            if (ret)
            {
                if (string.IsNullOrEmpty(this.CardPaymentInfo.TransactionToken))
                {
                    this.LastError = this.CreateError(LabelTranslator.Translate(75009), ErrorCode.InvalidTransaction);  
                    return false;
                }
            }

            return ret;
        }

        protected override IRequest PrepareRequest()
        {
            var resRequest = new ReversalBaseRequest();

            resRequest.InitFromCardPaymentInfo(this.CardPaymentInfo);
            resRequest.AnyPaySettings = this.ConnectionManager.ConnectionSettings;

            return resRequest;
        }

        protected override string FormHeaderLabel()
        {
            return LabelTranslator.Translate(75052); //Reversal: {0} {1}
        }
    }
}
