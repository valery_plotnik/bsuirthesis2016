﻿using System.Net.Sockets; 

namespace AP.AnyPayPOS.SocketTools
{
    public class TcpClientState
    {
        public byte[] Buffer { get; protected set; }
        public NetworkStream NetStream { get; protected set; } 

        public TcpClientState(NetworkStream netStream, byte[] buffer)
        {
            this.Buffer = buffer;
            this.NetStream  = netStream;  
        } 
    }
}
