﻿using System.Globalization;
using System.Threading;
using AP.Core.Contracts.Operation;
using AP.Core.Contracts.Operation.Error;

namespace AP.Core.Operation
{
    public abstract class PosOperation : IPosOperation
    {   
        public PosOperationStatus OperationStatus { get; protected set; }
        public virtual IPosOperationError LastError { get; protected set; }

        protected CultureInfo MainUiCulture { get; set; }  

        protected PosOperation()
        { 
            this.OperationStatus = PosOperationStatus.NotStarted;
            this.MainUiCulture = Thread.CurrentThread.CurrentUICulture;
        }

        public virtual void RunOperation()
        {
            this.PreExecute();
            this.Execute();
            this.PostExecute();
        }

        protected virtual void Execute()
        {
            this.SetUiCulture(); 
        }

        public void SetUiCulture()
        {
            Thread.CurrentThread.CurrentUICulture = this.MainUiCulture;
        }

        protected virtual void PreExecute()
        {
            
        }

        protected virtual void PostExecute()
        {
            
        } 

        public virtual void StopOperation()
        {

        }
    }
}
