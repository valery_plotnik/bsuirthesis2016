﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using AP.Logging;

namespace AP.AnyPayPOS.SocketTools
{
    public delegate void ResponseBytesReceived(byte[] responseBytes, int numberOfBytes);

    public class ConnectionChannelSettings
    {
        public IPAddress ServiceIpAddress { get; set; }
        public int ServicePortNumber { get; set; }
        public int BufferSize { get; set; }
        public int SendTimeoutSec { get; set; }
        public int ReceiveTimeoutSec { get; set; }

        public IPEndPoint ServiceEndPoint
        {
            get
            {
                var serviceEndPoint = new IPEndPoint(this.ServiceIpAddress, this.ServicePortNumber);
                return serviceEndPoint;
            }
        }
    }

    public class ConnectionChannel
    {
        public ConnectionChannelSettings Settings { get; set; }
        private TcpClientWrapper SocketClient { get; set; }
        private ConnectionState ConnectionState { get; set; }

        public event ResponseBytesReceived OnResponseBytesReceived; 
        public EventWaitHandle ReceiveFinishedEventHandle { get; protected set; }   

        public ConnectionChannel()
        {
            this.ConnectionState = new ConnectionState { IsClosed = false };
            this.ReceiveFinishedEventHandle = new EventWaitHandle(false, EventResetMode.AutoReset); 
        }

        internal void Connect()
        {
            SocketClient = this.GetSocketClient();
        }

        internal void SendRequest(byte[] requestBytes)
        {
            SocketClient = this.GetSocketClient();
            SocketClient.SendRequest(requestBytes);
        } 

        internal IAsyncResult ReceiveAsync()
        {
            this.ReceiveFinishedEventHandle.Reset(); 
            var result = this.GetSocketClient().BeginRead(this.ReadCallback);
            return result; 
        }

        internal void Close()
        {
            LoggerHandler.LogInfo("Before connection state lock");
            lock (this.ConnectionState)
            {
                if (this.ConnectionState.IsClosed)
                    return;

                this.ConnectionState.IsClosed = true;

                try
                { 
                    LoggerHandler.LogInfo("Closing socket client...");

                    if (SocketClient != null)
                    {
                        SocketClient.Close(true);
                    } 

                    LoggerHandler.LogInfo("Socket client is closed...");
                }
                catch (Exception exception)
                {
                    LoggerHandler.LogError(exception, this); //it's needed to close connection anyway but not to throw error  
                    LoggerHandler.LogInfo("Error occurred when closing socket client");
                }
            }
        }

        private TcpClientWrapper GetSocketClient()
        {
            if (this.SocketClient == null)
            {
                var tcpClient = new TcpClient
                {
                    SendTimeout = this.Settings.SendTimeoutSec*1000,
                    ReceiveTimeout = this.Settings.ReceiveTimeoutSec*1000
                };

                this.SocketClient = new TcpClientWrapper(tcpClient)
                {
                     BufferSize = this.Settings.BufferSize
                };
                this.SocketClient.Connect(this.Settings.ServiceEndPoint);
            }

            return this.SocketClient;
        }

        public void WaitWhileReceiveNotFinished()
        {
            this.ReceiveFinishedEventHandle.WaitOne();
        } 

        protected void ReadCallback(IAsyncResult asyncResult)
        {
            try
            {
                TcpClientState clientState = asyncResult.AsyncState as TcpClientState;
                int bytesReceived = clientState.NetStream.EndRead(asyncResult);

                if (bytesReceived > 0)
                {
                    this.ResponseBytesReceived(clientState.Buffer, bytesReceived);
                    this.GetSocketClient().BeginRead(this.ReadCallback);
                }
                else
                {
                    this.ReceiveFinishedEventHandle.Set();
                }
            }
            catch(Exception exception)
            {
                LoggerHandler.LogError(exception, this);
                this.ReceiveFinishedEventHandle.Set();
            } 
        }

        protected void ResponseBytesReceived(byte[] responseBytes, int numberOfReceivedBytes)
        {
            if (this.OnResponseBytesReceived != null)
            {
                this.OnResponseBytesReceived(responseBytes, numberOfReceivedBytes);
            }
        } 
    } 
}
