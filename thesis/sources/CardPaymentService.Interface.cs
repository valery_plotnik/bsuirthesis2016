﻿using System; 
using AP.Core;  
using AP.Core.Transaction;
using AP.Core.Xml; 
using AP.Logging;
using AP.POSProcesses.Transaction;
using AP.PXPPaymentContract;
using AP.PXPPaymentContract.DataEntity;
using AP.PXPPaymentContract.DataEntity.Info;
using AP.PXPPaymentContract.Services; 
using AP.PXPPaymentService.DataEntity;
using AP.PXPPaymentService.Extensions;
using AP.PXPPaymentService.Operation;
using AP.PXPServices;
using LSRetailPosis; 
using Microsoft.Dynamics.Retail.PaymentSDK;
using Microsoft.Dynamics.Retail.Pos.Contracts; 

namespace AP.PXPPaymentService
{ 
    public partial class CardPaymentService : AnyPayService, ICardPaymentService
    { 
        public IPropertiesManager PropertiesManager { get; protected set; } 

        public CardPaymentService(IApplication application, IAnyPaySettings anyPaySettings) : base(application, anyPaySettings)
        { 
            this.PropertiesManager = new PropertiesManager(); 
        } 

        public void Sale(ICardPaymentInfo cardPaymentInfo)
        {
            if (!this.ValidateTransaction(cardPaymentInfo))
                return;

            try
            {
                var operation = new SalesOperation 
                { 
                    CardPaymentInfo = cardPaymentInfo, 
                    TransactionInfo = this.CurrentTransactionInfo() 
                };

                this.RunOperation(operation); 

                cardPaymentInfo = operation.CardPaymentInfo;
                this.ProcessAuthorizationResults(cardPaymentInfo, operation, 75002, 75003);  /*Card payment is cancelled*/  /*Unknown error occurred during card payment*/
            }
            catch (Exception exception)
            {
                cardPaymentInfo.Errors.Add(new PaymentError(ErrorCode.ApplicationError, LabelTranslator.Translate(75003))); /*Unknown error occurred during card payment*/
                LoggerHandler.LogError(exception);
            }  
        }

        public void Refund(ICardPaymentInfo cardPaymentInfo)
        {
            if (!this.ValidateTransaction(cardPaymentInfo))
                return;

            try
            {
                var operation = new RefundOperation
                {
                    CardPaymentInfo = cardPaymentInfo,
                    TransactionInfo = this.CurrentTransactionInfo()
                };
                 
                this.RunOperation(operation); 

                cardPaymentInfo = operation.CardPaymentInfo;
                this.ProcessAuthorizationResults(cardPaymentInfo, operation, 75012, 75013); /*Refund was cancelled*/ /*Unknown error occurred during the refund processing*/ 
            }
            catch (Exception exception)
            {
                cardPaymentInfo.Errors.Add(new PaymentError(ErrorCode.ApplicationError, LabelTranslator.Translate(75013))); /*Unknown error occurred during the refund processing*/
                LoggerHandler.LogError(exception);
            }  
        }

        public void Reverse(ICardPaymentInfo cardPaymentInfo, ITransactionReversalInfo reversalInfo = null)
        {
            if (!this.ValidateTransaction(cardPaymentInfo))
                return;

            try
            {
                if (reversalInfo != null && reversalInfo.IsManualReverse)
                {
                    if (reversalInfo.OriginalAmount != cardPaymentInfo.PaymentAmount ||
                        reversalInfo.OriginalCurrency != cardPaymentInfo.Currency)
                    {
                        cardPaymentInfo.Errors.Add(new PaymentError(ErrorCode.InvalidAmount, 
                                                            string.Format(LabelTranslator.Translate(75020)+ "({0:0.00} {1})", 
                                                            reversalInfo.OriginalAmount, reversalInfo.OriginalCurrency))); //Reversal amount and currency must be equal to original payment amount and currency
                        return;
                    }
                }

                var operation = new ReversalOperation
                {
                    CardPaymentInfo = cardPaymentInfo,
                    TransactionInfo = PosTransactionStorage.Instance.CurrentTransaction.ToTransactionInfo()
                };
                 
                this.RunOperation(operation);  

                cardPaymentInfo = operation.CardPaymentInfo;
                this.ProcessAuthorizationResults(cardPaymentInfo, operation, 75010, 75011); /*Payment void was cancelled*/ /*Unknown error occurred during payment void*/ 
            }
            catch (Exception exception)
            {
                cardPaymentInfo.Errors.Add(new PaymentError(ErrorCode.ApplicationError, ApplicationLocalizer.Language.Translate(75011))); /*Unknown error occurred during payment void*/ 
                LoggerHandler.LogError(exception);
            }
        }

        public ICardPaymentInfo CreateCardPaymentInfo()
        {
            CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();
            return cardPaymentInfo;
        } 

        public ICardInfo CurrentPaymentCardInfo()
        {
            return PosTransactionStorage.Instance.CurrentPaymentCardInfo.ToCardInfo();
        }

        public ITransactionReversalInfo CreateTransactionReversalInfo()
        {
            ITransactionReversalInfo reversalInfo = new TransactionReversalInfo();
            return reversalInfo;
        }

        public string SerializeCardPaymentInfo(ICardPaymentInfo cardPaymentInfo)
        {
            var serializer = DataEntityXmlSerializer.CreateDefault();
            var xml = serializer.Serialize(cardPaymentInfo);  
            return xml;
        }

        public ICardPaymentInfo DeserializeCardPaymentInfoXml(string xml)
        {
            ICardPaymentInfo cardPaymentInfo = null;
            var serializer = DataEntityXmlSerializer.CreateDefault();
            var paymentInfoType = this.CreateCardPaymentInfo().GetType();
            cardPaymentInfo = serializer.Deserialize(xml, paymentInfoType) as ICardPaymentInfo; 
            return cardPaymentInfo;
        }

        public string SerializeTransactionReversalInfo(ITransactionReversalInfo reversalInfo)
        {
            var serializer = DataEntityXmlSerializer.CreateDefault();
            var xml = serializer.Serialize(reversalInfo);
            return xml;
        }

        public ITransactionReversalInfo DeserializeTransactionReversalInfo(string xml)
        {
            ITransactionReversalInfo reversalInfo = null;
            var serializer = DataEntityXmlSerializer.CreateDefault();
            var paymentInfoType = this.CreateTransactionReversalInfo().GetType();
            reversalInfo = serializer.Deserialize(xml, paymentInfoType) as ITransactionReversalInfo;
            return reversalInfo;
        }

        public void ClearReversalProperties(dynamic partnerData)
        {
            if (partnerData == null)
                return;

            partnerData.PXPReversalInfoXml = string.Empty;
        }

        public void ClearCurrentTransactionReversalProperties()
        {
            var partnerData = PosTransactionStorage.Instance.CurrentTransaction != null
                ? PosTransactionStorage.Instance.CurrentTransaction.PartnerData
                : null;

            this.ClearReversalProperties(partnerData);
        }

        public bool IsReverseCalledForCurrentTransaction()
        {
            var reversalInfo = this.GetCurrentTransationReversalInfo();

            return reversalInfo != null && reversalInfo.IsManualReverse;  
        }

        public ITransactionReversalInfo GetCurrentTransationReversalInfo()
        {
            var transactionReversalInfoObj = (PosTransactionStorage.Instance.CurrentTransaction.GetPartnerDataProperty(GlobalConstants.PxpReversalInfoPropertyName));

            string transactionReversalInfo = transactionReversalInfoObj as String;

            if (!string.IsNullOrEmpty(transactionReversalInfo))
            {
                var reversalInfo = this.DeserializeTransactionReversalInfo(transactionReversalInfo);
                return reversalInfo;
            }

            return this.CreateTransactionReversalInfo();
        } 
    }
}
