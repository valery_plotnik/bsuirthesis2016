﻿using System;
using System.Collections.Generic;
using AP.Core.Helpers;
using AP.PXPPaymentContract;
using AP.PXPPaymentContract.DataEntity;
using AP.PXPPaymentContract.DataEntity.Info;
using AP.PXPPaymentContract.Extensions;
using AP.PXPPaymentContract.PaymentProperties;
using LSRetailPosis;
using LSRetailPosis.Transaction.Line.TenderItem;
using Microsoft.Dynamics.Retail.PaymentSDK;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;

namespace AP.Core.Transaction
{
    public static class CardTenderLineItemExtensions
    {
        public static bool PartnerDataContainsProperty(this CardTenderLineItem cardTenderLineItem, string propertyName)
        {
            if (cardTenderLineItem == null)
                return false; 

            return DynamicObjectHelper.HasObjectProperty(cardTenderLineItem.PartnerData, propertyName);  
        }

        public static object GetPartnerDataProperty(this CardTenderLineItem cardTenderLineItem, string propertyName)
        {
            if (cardTenderLineItem == null || string.IsNullOrEmpty(propertyName))
                return null;

            return DynamicObjectHelper.GetObjectProperty(cardTenderLineItem.PartnerData, propertyName); 
        }

        public static List<PaymentProperty> PxpPaymentProperties(this CardTenderLineItem tenderLineItem)
        {
            List<PaymentProperty> properties = new List<PaymentProperty>();

            if (tenderLineItem == null || tenderLineItem.EFTInfo == null || string.IsNullOrEmpty(tenderLineItem.EFTInfo.PaymentProviderPropertiesXML))
                return properties;

            PaymentProperty[] paymentProviderPropertiesArray = PaymentProperty.ConvertXMLToPropertyArray(tenderLineItem.EFTInfo.PaymentProviderPropertiesXML);

            if (paymentProviderPropertiesArray == null)
                return properties;

            List<PaymentProperty> paymentProviderPropertiesList = new List<PaymentProperty>(paymentProviderPropertiesArray);

            PaymentProperty paymentProperty = paymentProviderPropertiesList.FindByName(IdentificationProperties.IsPxpPayment);

            if (paymentProperty != null && !string.IsNullOrEmpty(paymentProperty.Name))
            {
                properties.Add(paymentProperty);
            }

            paymentProperty = paymentProviderPropertiesList.FindByName(GlobalConstants.PxpPaymentInfoPropertyName);

            if (paymentProperty != null && !string.IsNullOrEmpty(paymentProperty.Name))
            {
                properties.Add(paymentProperty);
            }

            return properties;
        }

        public static ICardPaymentInfo PxpCardPaymentInfo(this CardTenderLineItem tenderLineItem)
        {
            ICardPaymentInfo cardPaymentInfo = AnyPayServices.PaymentService.CreateCardPaymentInfo();

            try
            {
                var pxpPaymentProperties = tenderLineItem.PxpPaymentProperties();

                if (pxpPaymentProperties == null || pxpPaymentProperties.Count == 0)
                    return null; 

                var paymentInfoProperty = pxpPaymentProperties.FindByName(GlobalConstants.PxpPaymentInfoPropertyName);

                if (paymentInfoProperty != null && !string.IsNullOrEmpty(paymentInfoProperty.Name))
                {
                    cardPaymentInfo = AnyPayServices.PaymentService.DeserializeCardPaymentInfoXml(paymentInfoProperty.StringValue); 
                }
            }
            catch (Exception ex)
            { 
                ApplicationLocalizer.Language.Translate(75001); /*Card payment details cannot be saved */
                ApplicationExceptionHandler.HandleException("Save PXP payment details", ex);
                Logging.LoggerHandler.LogError(ex);
            }  
            return cardPaymentInfo;
        } 

        public static ICardPaymentInfo PxpCardPaymentInfo(this ITenderLineItem tenderLineItem)
        {
            var cardItem = tenderLineItem as CardTenderLineItem;
            var paymentInfo = cardItem != null ? cardItem.PxpCardPaymentInfo() : null;

            return paymentInfo;
        }
    }
}
