﻿using System;   
using AP.AnyPayPOS.Message.Request;
using AP.AnyPayPOS.Message.Response;
using AP.Core.Contracts.Operation; 
using AP.Logging;
using AP.PXPPaymentContract;
using AP.PXPPaymentContract.DataEntity; 
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using AP.AnyPayPOS.Message;
using AP.Core; 
using AP.PXPPaymentContract.DataEntity.Info;
using AP.PXPPaymentContract.Message;
using AP.PXPPaymentContract.WinFormsTouch;
using AP.PXPServices;
using Microsoft.Dynamics.Retail.PaymentSDK;  

namespace AP.PXPPaymentService.Operation
{
    public class CardPaymentOperation : AnyPayOperation
    {
        public ICardPaymentInfo CardPaymentInfo
        {
            get { return this.AnyPayInfo as ICardPaymentInfo; }
            set { this.AnyPayInfo = value; }
        }

        public ITransactionInfo TransactionInfo { get; set; }

        protected TransBaseRequest TransactionBaseRequest
        {
            get { return this.Request as TransBaseRequest; }
        }

        public IPosTransaction PosTransaction { get; set; }

        protected override string GetUnknownErrorText()
        {
            return LabelTranslator.Translate(75003); /* Unknown error occurred during card payment */
        }

        protected virtual bool ValidateCardPaymentInfo()
        {
            if (this.CardPaymentInfo == null)
            {
                this.LastError = this.CreateError(LabelTranslator.Translate(75004));
                    /* Card payment information must be specified */
                return false;
            }

            return true;
        }

        protected override ICancellableStatusForm CreateForm()
        {
            return new PayCardStatusForm(this.CardPaymentInfo.PaymentAmount, this.CardPaymentInfo.Currency,
                this.FormHeaderLabel());
        }

        protected virtual string FormHeaderLabel()
        {
            return string.Empty;
        }

        protected override void RunAnyPayOperationSync()
        {
            try
            {
                this.SetUiCulture();
                this.AddMessageToForm(LabelTranslator.Translate(75005)); /* Connecting... */
                this.RunAnyPayCommunicationProcess();
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        }

        protected override void ProcessServiceResponse(IResponse response)
        {
            try
            {
                if (response == null)
                {
                    this.CardPaymentInfo.IsAuthorized = false;
                    this.SetOperationError();
                }
                else
                {
                    if (response is TransBaseResponse)
                    {
                        var transResponse = (TransBaseResponse) response;
                        this.InitCardPaymentInfoFromResponse(transResponse);
                    }
                }
            }
            catch (Exception exception)
            {
                this.SetErrorOperationStatus();
                LoggerHandler.LogError(exception, this);
            }
        }

        protected override void ConnectionManager_InfoMessageCreated(StatusMessageCodes messageCode)
        {
            this.SetUiCulture();

            if (messageCode != StatusMessageCodes.None)
            {
                var responseText = LabelTranslator.Translate(messageCode) ?? string.Empty;
                this.AddMessageToForm(responseText);
            }
        }

        protected override void ConnectionManager_ResponseReceived(IResponse response)
        {
            this.SetUiCulture();

            if (response is TransBaseResponse)
                //will be processed later. Only intermediate responses are processed by this method
                return;

            bool isAnyStatusCodeDisplayed = false;
            var statusCodes = response.StatusMessageCodeList;

            if (statusCodes != null && statusCodes.Count > 0)
            {
                foreach (var statusCode in statusCodes)
                {
                    if (statusCode != StatusMessageCodes.None)
                    {
                        isAnyStatusCodeDisplayed = true;
                        var responseText = LabelTranslator.Translate(statusCode) ?? string.Empty;
                        this.AddMessageToForm(responseText);
                    }
                }
            }

            if (!isAnyStatusCodeDisplayed)
            {
                this.AddMessageToForm(response.DisplayText);
            }
        }

        protected override void FormCancelButtonClicked()
        {
            this.CardPaymentInfo.ProcessingWasCancelled = true;
            this.StopAnyPayCommunication();
        }

        protected virtual KeyedTransBaseRequest BuildKeyedTransRequest()
        {
            var request = new KeyedTransBaseRequest();
            request.InitCardData(this.CardPaymentInfo.CardInfo);
            return request;
        }

        protected virtual IccTransBaseRequest BuildIccTransRequest()
        {
            if (this.CardPaymentInfo.AnyPaySettings.Credentials.PedDeviceType == AnyPayPedDeviceTypes.None)
            {
                this.LastError = this.CreateError(LabelTranslator.Translate(75006),
                    ErrorCode.InvalidMerchantConfiguration);
                    // PED is not configured. Please, validate POS settings in AX
                return null;
            }

            var request = new IccTransBaseRequest();
            return request;
        }

        protected void InitCardPaymentInfoFromResponse(TransBaseResponse baseResponse)
        {
            this.CardPaymentInfo.TransactionToken = baseResponse.Token;
            this.CardPaymentInfo.TransactionReference = baseResponse.TransactionReference;
            this.CardPaymentInfo.SetReceiptInfo(baseResponse.ReceiptInfo);

            if (this.TransactionBaseRequest != null)
            {
                this.CardPaymentInfo.ReceiptInfo.TransactionType = this.TransactionBaseRequest.TransactionType;
            }

            switch (baseResponse.ResponseCode)
            {
                case ResponseCodes.Authorised:
                case ResponseCodes.Approved:
                case ResponseCodes.ApprovedRangeSpecified:
                    this.CardPaymentInfo.IsAuthorized = true;
                    this.OperationStatus = PosOperationStatus.Success;
                    this.CardPaymentInfo.AuthorizationResult = AuthorizationResult.Success;
                    break;

                default:
                    this.CardPaymentInfo.IsAuthorized = false;
                    this.OperationStatus = PosOperationStatus.Fail;
                    this.CardPaymentInfo.AuthorizationResult = AuthorizationResult.Failure;
                    this.LastError =
                        this.CreateError(!string.IsNullOrEmpty(baseResponse.ResponseMessage) ? baseResponse.ResponseMessage : baseResponse.ResponseCode.ToString());
                    break;
            }
        }

        protected override bool ValidateSettings()
        {
            return base.ValidateSettings() && this.ValidateCardPaymentInfo();
        }
    }
}
